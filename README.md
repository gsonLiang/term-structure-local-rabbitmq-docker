# term-structure-local-rabbitmq-docker

## environment setup

| NAME | USAGE |
|------|-------|
|HOST_DIR|location to store data |
|RABBITMQ_DEFAULT_USER| rabbitmq user name |
|RABBITMQ_DEFAULT_PASS| rabbitmq password |
|RABBITMQ_ERLANG_COOKIE| rabbitmq cookie |